<?php
    /*
    Plugin Name: POSSync with WooCommerce
    Plugin URI:  https://s3.amazonaws.com/pos-sync-cdn/installers/pos-sync-clients/woocommerce/latest/possyncwoo.zip
    Description: This plugin allows POSSync to update inventory levels used by WooCommerce
    Author: Nahnay
    Version: 1.0.7
    Author URI:   https://www.pos-sync.com
    */

    // POSSync with WooCommerce
    /* Copyright 2014 Nahnay (email: dev@nahnay.com)   - MIT License

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
    documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
    Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
    OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    */


    // options code based on h1ttp://wordpress.org/extend/plugins/plugin-options-starter-kit/

    // Plugin Options Starter Kit copyright included here to satisfy license requirements
    /* Copyright 2009 David Gwyer (email : d.v.gwyer@presscoders.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
require_once(ABSPATH . 'wp-admin/includes/media.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/image.php');
require_once(ABSPATH . 'wp-admin/includes/taxonomy.php');
require_once(ABSPATH . 'wp-includes/post.php');
require_once(ABSPATH . 'wp-includes/kses.php');




/*if (!function_exists("wc_sanitize_taxonomy_name")){
    require_once(ABSPATH . "wp-content/plugins/woocommerce/includes/wc-formatting-functions.php");    
}
*/

// on older wordpress, add apache_request_headers
// special thanks to andre@shkalix.com for pointing this out
if(!function_exists('apache_request_headers')) { 
        function apache_request_headers() { 
                $headers = array(); 
                foreach($_SERVER as $key => $value) { 
                        if(substr($key, 0, 5) == 'HTTP_') { 
                                $headers[str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))))] = $value; 
                        } 
                } 
                return $headers; 
        } 
}

// $DEBUG_CODE_ID = uniqid(); // uncomment to enable debugging
function possync_is_debug(){
    return null;
}

// ------------------------------------------------------------------------
// MAIN ACTIONS
// ------------------------------------------------------------------------
// MAIN PLUGIN ACTIONS, BESIDES ADMIN FUNCTIONS
// ------------------------------------------------------------------------

// Handles POSSync requests
add_action( 'init', 'possync_handleAPIRequest' );
add_action( 'send_headers' , 'possync_sendheaders' );


// ------------------------------------------------------------------------
// REQUIRE MINIMUM VERSION OF WORDPRESS:
// ------------------------------------------------------------------------
// THIS IS USEFUL IF YOU REQUIRE A MINIMUM VERSION OF WORDPRESS TO RUN YOUR
// PLUGIN. IN THIS PLUGIN THE WP_EDITOR() FUNCTION REQUIRES WORDPRESS 3.3
// OR ABOVE. ANYTHING LESS SHOWS A WARNING AND THE PLUGIN IS DEACTIVATED.
// ------------------------------------------------------------------------

function requires_wordpress_version() {
    global $wp_version;
    $plugin = plugin_basename( __FILE__ );
    $plugin_data = get_plugin_data( __FILE__, false );

    if ( version_compare($wp_version, "3.3", "<" ) ) {
        if( is_plugin_active($plugin) ) {
            deactivate_plugins( $plugin );
            wp_die( "'".$plugin_data['Name']."' requires WordPress 3.3 or higher, and has been deactivated! Please upgrade WordPress and try again.<br /><br />Back to <a href='".admin_url()."'>WordPress admin</a>." );
        }
    }
}
function requires__plugin(){
    $foundWoo = False;
    foreach(apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) as $plugin_name){
        if (in_array("woocommerce.php", explode('/', $plugin_name) ) ){
            $foundWoo = True;
            break;
        }
    }
	if ( $foundWoo ) {
			//pass
	}else{

		$plugin = plugin_basename( __FILE__ );
		$plugin_data = get_plugin_data( __FILE__, false );
		$_plugin_path = "/.php";
	    deactivate_plugins($plugin);
	    wp_die( "'".$plugin_data['Name']."' requires the WooCommerce plugin, and has been deactivated! Please install and activate the WooCommerce plugin.<br /><br />Back to <a href='".admin_url()."'>WordPress admin</a>." );
	}
}

add_action( 'admin_init', 'requires_wordpress_version' );
add_action( 'admin_init', 'requires__plugin' );


// ------------------------------------------------------------------------
// REGISTER HOOKS & CALLBACK FUNCTIONS:
// ------------------------------------------------------------------------
// HOOKS TO SETUP DEFAULT PLUGIN OPTIONS, HANDLE CLEAN-UP OF OPTIONS WHEN
// PLUGIN IS DEACTIVATED AND DELETED, INITIALISE PLUGIN, ADD OPTIONS PAGE.
// ------------------------------------------------------------------------

// Set-up Action and Filter Hooks
register_activation_hook(__FILE__, 'possync_add_defaults');
register_uninstall_hook(__FILE__, 'possync_delete_plugin_options');
add_action('admin_init', 'possync_init' );
add_action('admin_menu', 'possync_add_options_page');
//add_filter( 'plugin_action_links', 'possync_plugin_action_links', 10, 2 );

// --------------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: register_uninstall_hook(__FILE__, 'possync_delete_plugin_options')
// --------------------------------------------------------------------------------------
// THIS FUNCTION RUNS WHEN THE USER DEACTIVATES AND DELETES THE PLUGIN. IT SIMPLY DELETES
// THE PLUGIN OPTIONS DB ENTRY (WHICH IS AN ARRAY STORING ALL THE PLUGIN OPTIONS).
// --------------------------------------------------------------------------------------

// Delete options table entries ONLY when plugin deactivated AND deleted
function possync_delete_plugin_options() {
    delete_option('possync_options');
}

// ------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: register_activation_hook(__FILE__, 'possync_add_defaults')
// ------------------------------------------------------------------------------
// THIS FUNCTION RUNS WHEN THE PLUGIN IS ACTIVATED. IF THERE ARE NO THEME OPTIONS
// CURRENTLY SET, OR THE USER HAS SELECTED THE CHECKBOX TO RESET OPTIONS TO THEIR
// DEFAULTS THEN THE OPTIONS ARE SET/RESET.
//
// OTHERWISE, THE PLUGIN OPTIONS REMAIN UNCHANGED.
// ------------------------------------------------------------------------------

// Define default option settings
function possync_add_defaults() {
    $tmp = get_option('possync_options');
    $apikey = get_option('possyncapi');
    if (!$apikey){
        possync_generateApiKey();
    }
}


// ------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: add_action('admin_init', 'possync_init' )
// ------------------------------------------------------------------------------
// THIS FUNCTION RUNS WHEN THE 'admin_init' HOOK FIRES, AND REGISTERS YOUR PLUGIN
// SETTING WITH THE WORDPRESS SETTINGS API. YOU WON'T BE ABLE TO USE THE SETTINGS
// API UNTIL YOU DO.
// ------------------------------------------------------------------------------

// Init plugin options to white list our options
function possync_init(){
    register_setting( 'possync_plugin_options', 'possync_options', 'possync_validate_options' );
}

// ------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: add_action('admin_menu', 'possync_add_options_page');
// ------------------------------------------------------------------------------
// THIS FUNCTION RUNS WHEN THE 'admin_menu' HOOK FIRES, AND ADDS A NEW OPTIONS
// PAGE FOR YOUR PLUGIN TO THE SETTINGS MENU.
// ------------------------------------------------------------------------------

// Add menu page
function possync_add_options_page() {
    add_options_page('POS-Sync + WooCommerce Options Options Page', 'POS-Sync + WooCommerce Options', 'edit_posts', __FILE__, 'possync_render_form');
}

// ------------------------------------------------------------------------------
// CALLBACK FUNCTION SPECIFIED IN: add_options_page()
// ------------------------------------------------------------------------------
// THIS FUNCTION IS SPECIFIED IN add_options_page() AS THE CALLBACK FUNCTION THAT
// ACTUALLY RENDER THE PLUGIN OPTIONS FORM AS A SUB-MENU UNDER THE EXISTING
// SETTINGS ADMIN MENU.
// ------------------------------------------------------------------------------

// Render the Plugin options form
function possync_render_form() {
    ?>
<div class="wrap">

    <!-- Display Plugin Icon, Header, and Description -->
    <div class="icon32" id="icon-options-general"><br></div>
    <h2>POS-Sync + WooCommerce Options</h2>

    <!-- Beginning of the Plugin Options Form -->
    <form method="post" action="options.php">
        <?php settings_fields('possync_plugin_options'); ?>
        <?php $apikey = get_option('possyncapi'); ?>

        <p></p><strong>API key: </strong><span><?php echo $apikey; ?></span></p>

        <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e('Generate new API key') ?>" />
        </p>
    </form>

</div>
<?php
}

function possync_validate_options($input) {
    possync_generateApiKey();
    return $input;
}

// Display a Settings link on the main Plugins page
function possync_plugin_action_links( $links, $file ) {

    if ( $file == plugin_basename( __FILE__ ) ) {
        $possync_links = '<a href="'.get_admin_url().'options-general.php?page=POSSyncWooCommerce/POSSyncWooCommerce.php">'.__('Settings').'</a>';
        // make the 'Settings' link appear first
        array_unshift( $links, $possync_links );
    }

    return $links;
}

// ------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: add_action('init', 'possync_handleAPIRequest' )
// ------------------------------------------------------------------------------
// THIS FUNCTION HANDLES POSSYNC API REQUESTS
// ------------------------------------------------------------------------------

function possync_handleAPIRequest(){
    if (isPOSSync()){
        if (!isset($_GET['fn'])){
            return false;
        }
        global $woocommerce;
        $fn = $_GET['fn'];
        $json = array();

        switch ($fn){
            case 'products':
                $json = possync_get_products();
                break;
            case 'product':
                if (!isset($_GET['id'])){
                    return false;
                }
                $id = $_GET['id'];
                array_push($json, possync_get_product($id));
                break;

            case 'update_product':
                $id = $_GET['id'];
                $quantity =  $_GET['quantity'];
                $json['status'] = possync_update_product($id,$quantity);
                break;
            case 'orders':
                $json = possync_get_orders();
                break;
            case 'add_products':
                $prods_data = null;
                $rawdata = null;
                $publish_state = "publish";
                if (isset($_GET['upload_state'])){
                    $publish_state = $_GET['upload_state'];
                }
                try{
                    $rawdata = file_get_contents('php://input');
                }catch(Exception $e){
                    $rawdata = null;
                }
                if ($rawdata){
                    try{
                        $prods_data = json_decode($rawdata);
                    }catch (Exception $e){
                        $prods_data = null;
                    }
                }
                if (!$prods_data){
                    var_dump("No data provided");
                    return false;
                }
                error_log("publish state for products before check: " . $publish_state);

                if (!array_search($publish_state, array("publish","draft"))){
                    $publish_state = "publish";
                }

                possync_add_products($prods_data, $publish_state);
                $json = array('status'=>'ok');
                break;
            case 'test':
                $json['key'] = "testkey";
                $json['value'] = "testvalue";
                break;
        }
        echo json_encode($json);
        die;
    }
}

function possync_sendheaders(){
    if (isPOSSync()){
        header('Content-type: application/json;');
    }
}


// ------------------------------------------------------------------------------
// Helper functions:
// ------------------------------------------------------------------------------
// Helper functions.
// ------------------------------------------------------------------------------
// Query string
// echo "<pre>"; print_r($wp_query->query_vars); echo "</pre>";

// Function to generate and save API key
function possync_generateApiKey(){
    $apikey = hash('sha256',get_current_user() . "\0" . microtime());
    update_option('possyncapi',$apikey);
}

// Checks to see if the page is called from POS-Sync
function isPOSSync(){
	$headers = apache_request_headers();
	foreach ($headers as $header => $value) {
		if (strtolower($header)=="possyncapi"){
			return possync_validateAPIKey($value);
		}
	}
	return false;
}

// Function to validate api key
function possync_validateAPIKey($apikey){
    if (!$apikey==get_option('possyncapi')){
        return false;
    }
    return true;
}

function possync_jsondumps($json){
    echo json_encode($json);
}


function remote_log($message){
    $debug_code_id = possync_is_debug();
    if ($debug_code_id===null){ return; }

    $url = 'https://bamserver.pos-sync.com/log';
    $data = array('debugid' => $debug_code_id, 'message' => $message, 'source'=>'POSSyncWooCommerce');
    $options = array(
            'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data),
        )
    );

    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
}

// ------------------------------------------------------------------------------
// Product functions.
// ------------------------------------------------------------------------------

function possync_variant_product($id, $name){
    // builds the variant product
    global $woocommerce;
    $product_factory = new WC_Product_Factory();
    $product_ = $product_factory->get_product($id);

    $product = array();
    $product['id'] = $id;
    $product['name'] = $name;
    $product['sku'] = $product_->get_sku();
    $product['quantity'] = $product_->get_stock_quantity();
    return $product;
}

// Creates the structure of a product for the return data
function possync_product($id){
	global $woocommerce;
	$product_factory = new WC_Product_Factory();
	$product_ = $product_factory->get_product($id);

    $product = array();
    $product['id'] = $id;
    $product['name'] = $product_->get_title();
    $product['sku'] = $product_->get_sku();
    $product['quantity'] = $product_->get_stock_quantity();

    if ($product_->has_child()){
        $product['subskus'] = array();
        foreach($product_->get_children() as $child_id){
            array_push($product['subskus'], $child_id);
        }
    }
    return $product;
}


function possync_get_product_by_sku($sku){
    global $wpdb;
    $product_id = $wpdb->get_var( $wpdb->prepare( "SELECT m.post_id as post_id FROM $wpdb->postmeta as m INNER JOIN wp_posts as p ON m.post_id = p.id WHERE m.meta_key='_sku' AND m.meta_value='%s' AND ( p.post_status='draft' OR p.post_status='publish') LIMIT 1", $sku ) );
    return $product_id;
}


// Return a dictionary of all HTML tags of a given kind and their attributes
// Borrowed from https://github.com/kdeldycke/e107-importer/blob/master/e107-importer.php
function possync_extract_html_tags($html_content, $tag_name, $allowed_protocols=array()) {
    // Default list of protocols copied from wp-includes/kses.php:wp_kses()
    if (empty($allowed_protocols))
      $allowed_protocols = array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet', 'mms', 'rtsp', 'svn');
    $tag_list = array();
    $tag_name = strtolower($tag_name);
    $tag_regexp = '/<\/?\s*'.$tag_name.'\s+(.+?)>/i';

    if (preg_match_all($tag_regexp, $html_content, $matches, PREG_SET_ORDER)) {
      foreach ($matches as $match) {
        // Parse attributes
        $attributes = array();
        foreach (wp_kses_hair($match[1], $allowed_protocols) as $attr)
          $attributes[$attr['name']] = $attr['value'];
        // Group all data of the tag in one array
        $tag_list[] = array( 'tag_string'       => $match[0]
                           , 'tag_name'         => $tag_name
                           , 'attribute_string' => $match[1]
                           , 'attributes'       => $attributes
                           );
      }
    }
    return $tag_list;
}

function possync_get_image_id($image_url) {
    global $wpdb;
    $img_name = basename($image_url, ".png");
    try{
        $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid LIKE '%s';", "%" . $img_name . "%" )); 
        return $attachment[0]; 
    }catch (Exception $e){
        //pass
        error_log("ERROR attaching image: " . $attachment->intl_get_error_message());
    }
    return null;
}


function possync_create_product($prod_data, $publish_state, $parent_id=""){
    // create the product post
    $post = array(
        'post_content' => $prod_data['description'],
        'post_status' => $publish_state,
        'post_title' => $prod_data['name'],
        'post_parent' => $parent_id,
        'post_type' => "product",
    );   
    if ($parent_id && strlen($parent_id)>0) {
        $post['post_type'] = 'product_variation';
    }
    $post_id = wp_insert_post($post);
    usleep(10000); //sleep for 0.1 seconds to allow write to db
    return $post_id;
}



function possync_add_product_variables($post_id, $prod_data){
    // Add to attributes

    $variable_count = 1;
    foreach($prod_data['options'] as $optionname=>$optionvalue){
        remote_log("Checking " . $optionname . '=' . $optionvalue);
        $arry_obj = get_post_meta( $post_id, '_product_attributes', true );
        if (!$arry_obj){
            $arry_obj = array();
        }
        error_log("existing product attributes");
        error_log(print_r($arry_obj,1));
        error_log("length of attributes array: " . count($arry_obj));
        $current_attribs = "";
        #if len TODO: array length test if zero add the attrib
        if (count($arry_obj)>0){

            foreach ($arry_obj as $attrib=>$value){
                error_log("processing attrib " . $attrib . " and optionname " . $optionname);
                if (strtolower($attrib)===strtolower($optionname)){
                    $current_attribs = $value['value'];
                    $attribs = array_map("trim", explode("|",$current_attribs));
                    error_log("Attribs length: " . count($attribs));

                    $optioncheck = false;
                    foreach($attribs as $checkattrib){
                        if ($checkattrib===$optionvalue){
                            $optioncheck = true;   
                        }
                    }
                    error_log("Found optionvalue in current attribs: " . ($optioncheck ? "yes" : "no"));

                    if ($optioncheck===false){
                        if (strlen($current_attribs)<1){
                            $current_attribs = $optionvalue;
                        }else{
                            $current_attribs = $current_attribs . " | " . trim($optionvalue);
                        }
                    }
                }else{
                    error_log($attrib . " does not equal " . $optionname);
                }
            }
        }

        if (strlen($current_attribs)<1){
            $current_attribs = trim($optionvalue);
        }
        // Didn't find an existing attribute for this optionname=>optionvalue so add it
        if (strlen($current_attribs)>0 ){
            $arry_obj[$optionname] = array("name"=>$optionname, "value"=>trim($current_attribs), "is_visible" => "1", "is_variation" => "1", "is_taxonomy"=>"0");
            error_log("Updating attributes list for " . $post_id);
            error_log(print_r($arry_obj,1));
            update_post_meta($post_id, "_product_attributes", $arry_obj);
        }
        
    }
}

function possync_add_product_metadata($post_id, $prod_data, $isParent){
    //global $woocommerce;

    // update meta data
    update_post_meta( $post_id, '_visibility', 'visible' );
    
    update_post_meta( $post_id, 'total_sales', '0');
    update_post_meta( $post_id, '_downloadable', 'no');
    update_post_meta( $post_id, '_virtual', 'no');
    update_post_meta( $post_id, '_regular_price', $prod_data['price'] );
    update_post_meta( $post_id, '_purchase_note', "" );
    update_post_meta( $post_id, '_featured', "no" );
    update_post_meta( $post_id, '_weight', "" );
    update_post_meta( $post_id, '_length', "" );
    update_post_meta( $post_id, '_width', "" );
    update_post_meta( $post_id, '_height', "" );
    update_post_meta( $post_id, '_sku', $prod_data['sku'] );
    update_post_meta( $post_id, '_price', $prod_data['price'] );
    update_post_meta( $post_id, '_sold_individually', "" );
    update_post_meta( $post_id, '_backorders', "no" );

    if ($isParent===true){
        update_post_meta( $post_id, '_manage_stock', "no" );
    }else{
        update_post_meta( $post_id, '_manage_stock', "yes" );    
        update_post_meta( $post_id, '_stock_status', 'instock');        
    }
    if (array_key_exists("quantity", $prod_data) && $isParent===false){
        update_post_meta( $post_id, '_stock', $prod_data['quantity'] );  
    } else{
        update_post_meta( $post_id, '_stock', 0 );  
    }

}


function possync_get_parent_category($category){
    // Gets the parent category if one exists
    global $wpdb;
    error_log("Getting parent category for category " . $category);
    $parent_id = $wpdb->get_var( $wpdb->prepare( "SELECT tt.parent as parent_id FROM $wpdb->term_taxonomy as tt INNER JOIN $wpdb->terms as t ON t.term_id = tt.term_id WHERE t.name='%s' AND tt.taxonomy='product_cat' LIMIT 1", $category ) );
    error_log("Parent id of " . $category . " is " . $parent_id);
    $parent_category = null;
    if ($parent_id>0){
        error_log("category has parent, getting it now...");
        // has parent
        $parent_category = $wpdb->get_var( $wpdb->prepare( "SELECT t.name as name FROM $wpdb->terms as t WHERE t.term_id=%s LIMIT 1", $parent_id ) );
        error_log("Parent category: " . $parent_category);
    }
    return $parent_category;
}


function set_categories_and_tags($post_id, $prod_data){

    $all_cats  = get_categories(array("taxonomy"=>'category', 'post_type'=>'product', 'hide_empty'=> 0));
    error_log("All categories:");
    error_log(print_r($all_cats,1));

    if (array_key_exists("categories", $prod_data)){
        $cats = wp_get_post_terms( $post_id, 'product_cat', array( 'fields' => 'names' ) );
        $category_parent = "";
        foreach($prod_data["categories"] as $category){
            // Add parent
            $parent_category = possync_get_parent_category($category);
            if (strlen($parent_category)>0 && !array_search($parent_category, $cats)){
                array_push($cats, $parent_category);
                wp_set_object_terms( $post_id, $cats, 'product_cat' );    
            }

            #$category_parent = wp_create_category($category, $category_parent);
            if (!array_search($category, $cats)){
                array_push($cats, $category);
                wp_set_object_terms( $post_id, $cats, 'product_cat' );    
            }
        }
    }

    if (array_key_exists("tags", $prod_data)){
        wp_set_post_tags( $post_id, $prod_data['tags'], true );
    }
}

function sideload_image($url, $post_id, $desc, $retries=10){


    // from http://codex.wordpress.org/Function_Reference/media_handle_sideload
    $tmp = download_url( $url );
    $file_array = array();
    // Set variables for storage
    // fix file filename for query strings
    preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches);
    $file_array['name'] = basename($matches[0]);
    $file_array['tmp_name'] = $tmp;

    // If error storing temporarily, unlink
    if ( is_wp_error( $tmp ) ) {
        error_log("Error storing temp file");
        error_log($tmp->get_error_message());
        @unlink($file_array['tmp_name']);
        $file_array['tmp_name'] = '';
    }

    // do the validation and storage stuff
    $id = media_handle_sideload( $file_array, $post_id, $desc );
    sleep(1);

    // Test for success
    $metadata = wp_get_attachment_metadata($id);
    if (count($metadata)===1){
        @unlink($file_array['tmp_name']);

        if ($retries>0){
            $retries = $retries -1;
            wp_delete_post($id); // cleanup
            error_log("Retrying sideload on " . $url);
            sideload_image($url, $post_id, $desc, $retries); // retry
        }else{
            error_log("Could not sideload image from " . $url);
            return null;
        }
    }

    // If error storing permanently, unlink
    if ( is_wp_error($id) ) {
        error_log("Error storing permanently");
        @unlink($file_array['tmp_name']);
        return $id;
    }
    @unlink($file_array['tmp_name']);

    //$src = wp_get_attachment_url( $id );
    
    return $id;
}

function attach_image($post_id, $prod_data){
    
    $image_id = null;
    if (array_key_exists("image_url", $prod_data)) {

        $image_id = possync_get_image_id($prod_data['image_url']);
        if (!$image_id){
            try{
                $image_id = sideload_image($prod_data['image_url'], $post_id, $prod_data['name']);
                //$image_html = media_sideload_image($prod_data['image_url'], $post_id, $prod_data['name']);
            }catch (Exception $e){
                error_log("Could not sideload image from " . $prod_data['image_url']);
                //could not get image
            }
        }
    }
    return $image_id;

}

// Gets or creates the product and updates with prod_data
function possync_get_or_set_product($prod_data, $publish_state){

    global $woocommerce;
    if (is_array($prod_data)){
        // noop, just use it
    }elseif(is_object($prod_data)){
        //construct array from object to normalize functions
        $ary = get_object_vars ($prod_data);
        $prod_data = array();
        $prod_data = array_merge($ary);
    }

    $product_factory = new WC_Product_Factory();
    $post_id = possync_get_product_by_sku($prod_data['sku']);

    $parent_id = null;
    $product_has_options = false;
    if (
        array_key_exists('options', $prod_data)                 // key exists?
        && $prod_data['options']!==null                         // options is not null
        && count(get_object_vars($prod_data['options']))>0       // object count is > 0
        ){
        $product_has_options = true;
    }
    error_log("creating " . ($product_has_options ? "variable":"simple") . " product");

    if ($product_has_options){
        // create the parent product
        $parent_sku = str_replace(" ", "", $prod_data['name']);
        $parent_prod_data = array_merge($prod_data);
        $parent_prod_data['sku'] = $parent_sku;
        $parent_id = possync_get_product_by_sku($parent_sku);
        if (!$parent_id){
            $parent_id = possync_create_product($prod_data, $publish_state);
        }else{
            $updated_post = array(
                'ID'=>$parent_id,
                'post_status'=>$publish_state
            );
            wp_update_post($updated_post);
        }
        possync_add_product_variables($parent_id, $prod_data, true);

        set_categories_and_tags($parent_id, $prod_data);

        // Setup parent id as variable product
        wp_set_object_terms ($parent_id, 'variable', 'product_type');
        possync_add_product_metadata($parent_id, $parent_prod_data, true);

        if ($post_id){
            $updated_post = array(
                'ID'=>$post_id,
                'post_parent'=>$parent_id,
                'post_type'=>'product_variation',
                'post_status'=>$publish_state
            );
            wp_update_post($updated_post);
        }
    }

    if (!$post_id){
        // Check for a variable product - one with options
        if ($product_has_options && $parent_id){
            error_log("Creating the variable product...");
            $post_id = possync_create_product($prod_data, "publish", $parent_id);  //all variants must be in published state
         }else{
            //create the simple product
            error_log("Creating the simple product...");
            $post_id = possync_create_product($prod_data, $publish_state);
            
        }
    }
    set_categories_and_tags($post_id, $prod_data);

    possync_add_product_metadata($post_id, $prod_data, false);
    
    error_log("product has options: " . ($product_has_options ? "yes" : "no"));
    if ($product_has_options){
        foreach($prod_data['options'] as $opt=>$val)   {
            error_log("updateding post meta with 'attribute_" . strtolower($opt) . "' with value [" . $val . "]");
            update_post_meta($post_id, "attribute_" . strtolower($opt), $val);
        }
    }else{
        // noop
    }


    sleep(1);

    // Side load the image
    //$target_post_id = $parent_id ? $parent_id:$post_id;

    #return array("post_id"=>$target_post_id, "data"=>$prod_data);
    return array("post_id"=>$post_id, "data"=>$prod_data);
}

// Gets all products
function possync_get_products(){
    global $woocommerce;

    $args = array('post_type' => 'product', 'post_status' => 'publish', 'posts_per_page' => -1);
    $products = array();
    $posts = get_posts($args);
    foreach ($posts as $post ) : setup_postdata( $post );
    
        $product_factory = new WC_Product_Factory();
        $product_ = $product_factory->get_product($post->ID);

        $product = array();
        if ($product_->has_child()){
            foreach ($product_->get_children() as $child){
                array_push($products, possync_variant_product($child, $product_->get_title()));
            }
        }else{
            array_push($products, possync_product($post->ID));    
        }
    endforeach;
    return $products;
}

// Gets a product by id
function possync_get_product($id){
	return possync_product($id);
}

// Updates a product
function possync_update_product($id, $quantity){
	global $woocommerce;
	$product_factory = new WC_Product_Factory();
	$product_ = $product_factory->get_product($id);
	$product_->set_stock($quantity);
	return true;
}

function add_query_vars_filter( $vars ){
  $vars[] = "from";
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );


function get_order_subtotal( $order ) {
    // Direct copy from WooCommerce 2.1 REST API

    $subtotal = 0;
    foreach ( $order->get_items() as $item ) {
        $subtotal += ( isset( $item['line_subtotal'] ) ) ? $item['line_subtotal'] : 0;
    }
    return $subtotal;
}

function get_order($id, $from, $fields = null ) {
    global $woocommerce;

    // Modified from WooCommerce 2.1 REST API
    // ensure order ID is valid & user has permission to read
    #$id = $this->validate_request( $id, 'shop_order', 'read' );

    if ( is_wp_error( $id ) )
        return $id;

    $order = new WC_Order( $id );

    $order_post = get_post( $id );
   
    if ($from){
        if (!$order_post->post_modified_gmt) return null;
        if (strtotime($order_post->post_modified_gmt)-$from<0) return null;
    }

    if (!function_exists("wc_format_decimal")){
        // Some woo commerce installs don't have this function
        function wc_format_decimal( $number, $dp = false, $trim_zeros = false ) {
            // Remove locale from string
            if ( ! is_float( $number ) ) {
                    $locale   = localeconv();
                    $decimals = array( get_option( 'woocommerce_price_decimal_sep' ), $locale['decimal_point'], $locale['mon_decimal_point'] );
                    $number   = sanitize_text_field( str_replace( $decimals, '.', $number ) );
            }

            // DP is false - don't use number format, just return a string in our format
            if ( $dp !== false ) {
                    $dp     = intval( $dp == "" ? get_option( 'woocommerce_price_num_decimals' ) : $dp );
                    $number = number_format( floatval( $number ), $dp, '.', '' );
            }

            if ( $trim_zeros && strstr( $number, '.' ) ) {
                    $number = rtrim( rtrim( $number, '0' ), '.' );
            }

            return $number;
        }

    }

    $order_data = array(
        'id'                        => $order->id,
        'order_number'              => $order->get_order_number(),
        'created_at'                => $order_post->post_date_gmt,//->format('Y-m-d H:i:s'),
        'updated_at'                => $order_post->post_modified_gmt ,
        'completed_at'              => $order->completed_date,
        'status'                    => $order->status,
        'total'                     => wc_format_decimal( $order->get_total(), 2 ),
        'subtotal'                  => wc_format_decimal( get_order_subtotal( $order ), 2 ),
        'total_line_items_quantity' => $order->get_item_count(),
        'total_tax'                 => wc_format_decimal( $order->get_total_tax(), 2 ),
        'total_shipping'            => wc_format_decimal( $order->order_shipping, 2 ),
        'cart_tax'                  => wc_format_decimal( $order->order_tax, 2 ),
        'shipping_tax'              => wc_format_decimal( $order->get_shipping_tax(), 2 ),
        'total_discount'            => wc_format_decimal( $order->get_total_discount(), 2 ),
        'cart_discount'             => wc_format_decimal( $order->get_cart_discount(), 2 ),
        'order_discount'            => wc_format_decimal( $order->get_order_discount(), 2 ),
        'shipping_methods'          => $order->get_shipping_method(),
        'payment_details' => array(
            'method_id'    => $order->payment_method,
            'method_title' => $order->payment_method_title,
            'paid'         => isset( $order->paid_date ),
        ),
        'billing_address' => array(
            'first_name' => $order->billing_first_name,
            'last_name'  => $order->billing_last_name,
            'company'    => $order->billing_company,
            'address_1'  => $order->billing_address_1,
            'address_2'  => $order->billing_address_2,
            'city'       => $order->billing_city,
            'state'      => $order->billing_state,
            'postcode'   => $order->billing_postcode,
            'country'    => $order->billing_country,
            'email'      => $order->billing_email,
            'phone'      => $order->billing_phone,
        ),
        'shipping_address' => array(
            'first_name' => $order->shipping_first_name,
            'last_name'  => $order->shipping_last_name,
            'company'    => $order->shipping_company,
            'address_1'  => $order->shipping_address_1,
            'address_2'  => $order->shipping_address_2,
            'city'       => $order->shipping_city,
            'state'      => $order->shipping_state,
            'postcode'   => $order->shipping_postcode,
            'country'    => $order->shipping_country,
        ),
        'note'                      => $order->customer_note,
        'customer_id'               => $order->customer_user,
        'line_items'                => array(),
        'shipping_lines'            => array(),
        'tax_lines'                 => array(),
        'fee_lines'                 => array(),
        'coupon_lines'              => array(),
    );


    // add line items
    foreach( $order->get_items() as $item_id => $item ) {

        $product = $order->get_product_from_item( $item );
        if ($product===null || gettype($product)!="object"){ continue; }

        $product_id = null;
        error_log("product: [" . gettype($product) . "]");
        try{
            $product_id = ( isset( $product->variation_id ) ) ? $product->variation_id : $product->id;
        }catch (Exception $e){
            error_log($e);
        }

        $order_data['line_items'][] = array(
            'id'         => $item_id,
            'subtotal'   => wc_format_decimal( $order->get_line_subtotal( $item ), 2 ),
            'total'      => wc_format_decimal( $order->get_line_total( $item ), 2 ),
            'total_tax'  => wc_format_decimal( $order->get_line_tax( $item ), 2 ),
            'price'      => wc_format_decimal( $order->get_item_total( $item ), 2 ),
            'quantity'   => (int) $item['qty'],
            'tax_class'  => ( ! empty( $item['tax_class'] ) ) ? $item['tax_class'] : null,
            'name'       => $item['name'],
            'product_id' => $product_id,
            'sku'        => is_object( $product ) ? $product->get_sku() : null,
        );
    }

    // add shipping
    foreach ( $order->get_items( 'shipping' ) as $shipping_item_id => $shipping_item ) {

        $order_data['shipping_lines'][] = array(
            'id'           => $shipping_item_id,
            'method_id'    => $shipping_item['method_id'],
            'method_title' => $shipping_item['name'],
            'total'        => wc_format_decimal( $shipping_item['cost'], 2 ),
        );
    }

    // add taxes
    foreach ( $order->get_tax_totals() as $tax_code => $tax ) {

        $order_data['tax_lines'][] = array(
            'code'     => $tax_code,
            'title'    => $tax->label,
            'total'    => wc_format_decimal( $tax->amount, 2 ),
            'compound' => (bool) $tax->is_compound,
        );
    }

    // add fees
    foreach ( $order->get_fees() as $fee_item_id => $fee_item ) {

        $order_data['fee_lines'] = array(
            'id'        => $fee_item_id,
            'title'     => $fee_item['name'],
            'tax_class' => ( ! empty( $fee_item['tax_class'] ) ) ? $fee_item['tax_class'] : null,
            'total'     => wc_format_decimal( $order->get_line_total( $fee_item ), 2 ),
            'total_tax' => wc_format_decimal( $order->get_line_tax( $fee_item ), 2 ),
        );
    }

    // add coupons
    foreach ( $order->get_items( 'coupon' ) as $coupon_item_id => $coupon_item ) {

        $order_data['coupon_lines'] = array(
            'id'     => $coupon_item_id,
            'code'   => $coupon_item['name'],
            'amount' => wc_format_decimal( $coupon_item['discount_amount'], 2 ),
        );
    }

    return array( 'order' => apply_filters( 'woocommerce_api_order_response', $order_data, $order, $fields) );

}


// Gets the orders
function possync_get_orders(){
    global $woocommerce;

    $args = array(
        'post_type' => 'shop_order', 
        'posts_per_page' => get_query_var('per_page'), 
        'paged' => get_query_var('page')
    );

    $from = null;
    if (isset($_GET['from'])){
        //$from  = date_parse($_GET['from']);
        try{
            $from = strtotime($_GET['from']);
        }catch (Exception $e){
            $from = null;
        }
    }

    $orders = array();
    
    $posts = get_posts($args);
    
    foreach ($posts as $post ) : setup_postdata( $post );
        $order = get_order($post->ID, $from);
        if ($order!=null){
            array_push($orders, $order);
        }

    endforeach;
    return $orders;

}


// Adds products from an array
function possync_add_products($data, $publish_state){
    global $woocommerce;

    $posts = array();
    foreach($data as $prod_data){
        $posts[] = possync_get_or_set_product($prod_data, $publish_state);
    }

    $imgposts = array();
    foreach($posts as $post_details){
        $post_id = $post_details['post_id'];
        $prod_data = $post_details['data'];
        $image_id = attach_image($post_id, $prod_data);
        array_push($imgposts, array("image_url"=>$prod_data['image_url'], "post_id"=>$post_id));
        sleep(1);
    }

    foreach($imgposts as $imgpost){
        $image_id = possync_get_image_id($imgpost['image_url']);
        update_post_meta( $imgpost['post_id'], '_thumbnail_id', $image_id );       
        update_post_meta( $imgpost['post_id'], '_image', $image_id );   

        $post = get_post($post_id);
        if ( $post->post_parent && $image_id){
            update_post_meta( $post->post_parent, '_thumbnail_id', $image_id );       
            update_post_meta( $post->post_parent, '_image', $image_id );   
        }
    }

    foreach($posts as $post_details){
        // force a final update
        $updated_post = array(
            'ID'=>$post_details['post_id'],
            'post_status'=>$publish_state
        );
        wp_update_post($updated_post);        
    }
}



