=== WooCommerce POS-Sync ===
Contributors: Nahnay, LLC
Tags: woocommerce, possync
Stable tag: 1.0.7

WooCommerce Plugin to enable inventory synchronization with POS-Sync (https://www.pos-sync.com)

== Description ==

WooCommerce Plugin to enable inventory synchronization with POS-Sync (https://www.pos-sync.com)

== Installation ==

1. Upload the entire 'woocommerce-possync' folder to the '/wp-content/plugins/' directory or install from the Admin plugins menu
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Click on settings in the "POSSync with WooCommerce" plugin
4. Use the full host name of your WooCommerce site and the API key displayed on the plugin page at https://www.pos-sync.com/profile/appsettings

== Changelog ==

= 1.0.7 =
Bug fix on check for null order object

= 1.0.6 =
Made calls compatible with WC2.0

= 1.0.5 =
Added all wp require_once lines.

= 1.0.4 =
Added support for product uploads.

= 1.0.3 =
Fixed bug when in Woo version < 2.1 where query_per_page was returning too few entries

= 1.0.2 =
Bug fixes

= 1.0.1 =
* Added getting product variants
* Added getting of orders

= 1.0.0 =
* Initial Release