@echo off
SET THIS_VER=1.0.7
SET ZIPFILE=possyncwoo.zip
SET PLATFORM=woocommerce
SET DIST_FILE="C:\workarea\possync-woocommerce\plugin\%ZIPFILE%" 
echo Deploying %ZIPFILE%...

del %DIST_FILE%

:: Zip it, zip it good
7za a %DIST_FILE% possyncwoo.php
7za a %DIST_FILE% readme.txt
7za a %DIST_FILE% woocommerce-template.php

"\Program Files\Microsoft Security Client\MpCmdRun.exe" -Scan -ScanType 3 -File %DIST_FILE%
if not errorlevel==0 goto BADSCAN
echo %THIS_VER% > version.txt
s3 put  pos-sync-cdn/installers/pos-sync-clients/%PLATFORM%/latest/ version.txt /acl:public-read
s3 put  pos-sync-cdn/installers/pos-sync-clients/%PLATFORM%/latest/ %DIST_FILE% /acl:public-read
s3 put  pos-sync-cdn/installers/pos-sync-clients/%PLATFORM%/older/%THIS_VER%/ %DIST_FILE% /acl:public-read

echo Deployed file %DIST_FILE%

GOTO END

:BADSCAN
echo Security Scan Failed

:END
